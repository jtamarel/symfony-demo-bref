clean-s3:
	aws s3 rm 's3://jta-symfony-demo-dev-serverlessdeploymentbucket-7ejcl3sv49wo/public/build' \
		--recursive

deploy-s3:
	aws s3api put-bucket-cors --bucket='jta-symfony-demo-dev-serverlessdeploymentbucket-7ejcl3sv49wo' \
		--cors-configuration='file://aws/s3-cors-config.json'
	aws s3 sync public/build 's3://jta-symfony-demo-dev-serverlessdeploymentbucket-7ejcl3sv49wo/public/build' \
		--follow-symlinks \
		--acl=public-read \
		--cache-control='max-age:60,public' \
		--delete

deploy-lambda:
	php vendor/bin/bref deploy


deploy: deploy-lambda deploy-s3
